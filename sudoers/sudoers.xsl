<?xml version="1.0" encoding="UTF-8"?>
<!--
Author: Sumit Bose <sbose@redhat.com>

Copyright (C) 2008  Red Hat
see file 'COPYING' for use and warranty information

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; version 2 only

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.pnp-software.com/XSLTdoc"
  xmlns:md="http://freeipa.org/xsl/metadata/1.0"
  xmlns:sudoers="http://freeipa.org/xml/rng/sudo/1.0"
                  xmlns:exsl="http://exslt.org/common"
                  extension-element-prefixes="exsl">


  <md:output_handler>
    <md:file md:name="/etc/sudoers" md:owner="root" md:group="root" md:permission="0440"/>
  </md:output_handler>

  <xsl:param name="merge"/>
  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="$merge!=''">
        <exsl:document href="-" method="xml" indent="yes">
          <xsl:call-template name="sudoers:merge">
            <xsl:with-param name="file_list" select="$merge"/>
          </xsl:call-template>
        </exsl:document>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text># IPA generated /etc/sudoers: DO NOT EDIT&#xA;&#xA;</xsl:text>
        <xsl:apply-templates select="sudoers:ipa"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="sudoers:ipa">
    <xsl:apply-templates>
      <xsl:with-param name="sudoers:ipaconfig"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="sudoers:ipaconfig">
    <xsl:apply-templates>
      <xsl:with-param name="sudoers:sudoers" select="''"/>
    </xsl:apply-templates>
  </xsl:template>


  <xsl:template match="sudoers:metadata">
  </xsl:template>

  <xd:doc>
    <xd:short>This is the short description with <code>HTML tags</code>.</xd:short>
    <xd:detail>
    And here comes a <b>more detailed</b> 
    description showed only in the detailed view of the documentation.
    </xd:detail>
  </xd:doc>
  <xsl:template match="sudoers:sudoers">
    <xsl:variable name="name">
      <xsl:apply-templates select="sudoers:subject">
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:apply-templates select="sudoers:command|sudoers:option">
      <xsl:with-param name="name" select="$name"/>
    </xsl:apply-templates>
  </xsl:template>



  <xsl:template match="sudoers:subject">
    <xsl:call-template name="format_name">
      <xsl:with-param name="name" select="sudoers:name"/>
      <xsl:with-param name="type" select="sudoers:type"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="sudoers:option">
    <xsl:param name="name"/>
    <xsl:text>Default</xsl:text>
    <xsl:choose>
      <xsl:when  test="$name = 'ALL'">
        <xsl:text> </xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>:</xsl:text>
        <xsl:value-of select="$name"/>
        <xsl:text> </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:for-each select="*">
      <xsl:choose>
        <!-- boolean options -->
        <xsl:when test="name()='always_set_home' or
                        name()='authenticate' or
                        name()='env_editor' or
                        name()='env_reset' or
                        name()='fqdn' or
                        name()='ignore_dot' or
                        name()='ignore_local_sudoers' or
                        name()='insults' or
                        name()='log_host' or
                        name()='log_year' or
                        name()='long_otp_prompt' or
                        name()='mail_always' or
                        name()='mail_badpass' or
                        name()='mail_no_host' or
                        name()='mail_no_perms' or
                        name()='mail_no_user' or
                        name()='noexec' or
                        name()='path_info' or
                        name()='passprompt_override' or
                        name()='preserve_groups' or
                        name()='requiretty' or
                        name()='root_sudo' or
                        name()='rootpw' or
                        name()='runaspw' or
                        name()='set_home' or
                        name()='set_logname' or
                        name()='setenv' or
                        name()='shell_noargs' or
                        name()='stay_setuid' or
                        name()='targetpw' or
                        name()='tty_tickets'
                        ">
          <xsl:if test=". = 'off'">
            <xsl:text>!</xsl:text>
          </xsl:if>
          <xsl:value-of select="name()"/>
        </xsl:when>

        <!-- integer option -->
        <xsl:when test="name()='passwd_tries'" >
          <xsl:value-of select="name()"/>
          <xsl:text>=</xsl:text>
          <xsl:value-of select="."/>
        </xsl:when>

        <!-- integer/booleans option, we handle them like integers -->
        <xsl:when test="name()='loglinelen' or
                        name()='passwd_timeout' or
                        name()='timestamp_timeout' or
                        name()='umask'
                        ">
          <xsl:value-of select="name()"/>
          <xsl:text>=</xsl:text>
          <xsl:value-of select="."/>
        </xsl:when>

        <!-- string options -->
        <xsl:when test="name()='badpass_message' or
                        name()='editor' or
                        name()='mailsub' or
                        name()='noexec_file' or
                        name()='passprompt' or
                        name()='role' or
                        name()='runas_default' or
                        name()='syslog_badpri' or
                        name()='syslog_goodpri' or
                        name()='timestampdir' or
                        name()='timestampowner' or
                        name()='type'
                        ">
          <xsl:value-of select="name()"/>
          <xsl:text>="</xsl:text>
          <xsl:value-of select="."/>
          <xsl:text>"</xsl:text>
        </xsl:when> 
        <!-- string/boolean options -->
        <xsl:when test="name()='exempt_group' or
                        name()='lecture' or
                        name()='lecture_file' or
                        name()='listpw' or
                        name()='logfile' or
                        name()='mailerflags' or
                        name()='mailerpath' or
                        name()='mailto' or
                        name()='syslog' or
                        name()='verifypw'
                        ">
          <xsl:choose>
            <xsl:when test=".='off'">
              <xsl:text>!</xsl:text>
              <xsl:value-of select="name()"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="name()"/>
              <xsl:text>="</xsl:text>
              <xsl:value-of select="."/>
              <xsl:text>"</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>

        <!-- list/boolean options -->
        <xsl:when test="name()='env_check' or
                        name()='env_delete' or
                        name()='env_keep'
                        ">
          <xsl:choose>
            <xsl:when test=".='off'">
              <xsl:text>!</xsl:text>
              <xsl:value-of select="name()"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="name()"/>
              <xsl:text>="</xsl:text>
              <xsl:value-of select="."/>
              <xsl:text>"</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="sudoers:command">
    <xsl:param name="name"/>
    <xsl:variable name="command" select="sudoers:path"/>
    <xsl:variable name="runas" select="sudoers:runas"/>
    <xsl:variable name="tag">
      <xsl:call-template name="format_tag">
        <xsl:with-param name="tag" select="sudoers:tag"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:value-of select="$name"/>
    <xsl:text> ALL = </xsl:text>
    <xsl:if test="$runas != ''">
      <xsl:text>(</xsl:text>
      <xsl:value-of select="$runas"/>
      <xsl:text>) </xsl:text>
    </xsl:if>
    <xsl:if test="$tag != ''">
      <xsl:value-of select="$tag"/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:value-of select="$command"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template name="format_name">
    <xsl:param name="name"/>
    <xsl:param name="type"/>

    <xsl:choose>
      <xsl:when test="$type = 'ALL'">
        <xsl:text>ALL</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$type = 'netgroup'">
            <xsl:text>+</xsl:text>
          </xsl:when>
          <xsl:when test="$type = 'posixGroup'">
            <xsl:text>%</xsl:text>
          </xsl:when>
        </xsl:choose>
        <xsl:value-of select="$name"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="format_tag">
    <xsl:param name="tag"/>

    <xsl:if test="$tag != ''">
      <xsl:value-of select="$tag"/>
      <xsl:text>:</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template name="sudoers:merge">
    <xsl:param name="file_list"/>
    <xsl:choose>
      <xsl:when test="contains($file_list,' ')">
      <!-- From sudoers(5): Where there are multiple matches, the last match is
           used (which is not necessarily the most specific match).
           This means we have to print the content of the files in file_list in
           reverse order. -->
        <xsl:call-template name="sudoers:merge">
          <xsl:with-param name="file_list" select="substring-after($file_list,' ')"/>
        </xsl:call-template>
        <xsl:variable name="file">
          <xsl:value-of select="substring-before($file_list,' ')"/>
        </xsl:variable>
        <xsl:copy-of select="document($file)/sudoers:ipa/sudoers:ipaconfig/*"/>
        <xsl:text>&#x0A;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="document($file_list)/sudoers:ipa/sudoers:ipaconfig/*"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
