<?xml version="1.0" encoding="UTF-8"?>
<!--
Author: Sumit Bose <sbose@redhat.com>

Copyright (C) 2008  Red Hat
see file 'COPYING' for use and warranty information

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; version 2 only

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:md="http://freeipa.org/xsl/metadata/1.0"
  xmlns:xd="http://www.pnp-software.com/XSLTdoc"
  xmlns:pol="http://freeipa.org/xml/rng/policykit_roles/1.0">

  <md:output_handler>
    <file name="/tmp/policykit-SAFE.ldif" owner="root" group="root" permission="400"/>
  </md:output_handler>

  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:text># IPA generated ldif for policykit roles. DO NOT EDIT&#xA;&#xA;</xsl:text>
    <xsl:apply-templates select="pol:ipa"/>
  </xsl:template>

  <xsl:template match="pol:ipa">
    <xsl:apply-templates>
      <xsl:with-param name="pol:iparole"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="pol:metadata">
  </xsl:template>

  <xsl:template match="pol:iparole">
      <xsl:apply-templates select="pol:role"/>
  </xsl:template>

  <xsl:template match="pol:role">
    <xsl:text># role: </xsl:text>
    <xsl:value-of select="pol:name"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>dn: ou=</xsl:text>
    <xsl:value-of select="pol:name"/>
    <xsl:text>,ou=PolicyKitRoles,xx=some,xx=ldap,xx=path&#xA;&#xA;&#xA;</xsl:text>

    <xsl:apply-templates select="pol:action">
      <xsl:with-param name="rolename" select="pol:name"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="pol:action">
    <xsl:param name="rolename"/>

    <xsl:text>dn: ou=</xsl:text>
    <xsl:value-of select="pol:action_id"/>
    <xsl:text>,ou=</xsl:text>
    <xsl:value-of select="$rolename"/>
    <xsl:text>,ou=PolicyKitRoles,xx=some,xx=ldap,xx=path&#xA;</xsl:text>
    <xsl:text>changetype: modify&#xA;</xsl:text>
    <xsl:text>replace: allow_any&#xA;allow_any: </xsl:text>
    <xsl:value-of select="pol:allow_any"/>
    <xsl:text>&#xa;-&#xa;changetype: modify&#xA;</xsl:text>
    <xsl:text>replace: allow_inactive&#xA;allow_inactive: </xsl:text>
    <xsl:value-of select="pol:allow_inactive"/>
    <xsl:text>&#xa;-&#xa;changetype: modify&#xA;</xsl:text>
    <xsl:text>replace: allow_active&#xA;allow_active: </xsl:text>
    <xsl:value-of select="pol:allow_active"/>
    <xsl:text>&#xa;&#xA;</xsl:text>
  </xsl:template>


  <xsl:template match="pol:file">
    <xsl:choose>
      <xsl:when test="name(./*[1])='url'">
        <xsl:text>su - nobody 'curl -o /tmp/SAFE_TEMP_FILE </xsl:text>
        <xsl:value-of select="pol:url"/>
        <xsl:text>'&#xA;</xsl:text>
      </xsl:when>
      <xsl:when test="name(./*[1])='data'">
        <xsl:text>cat &#x3C;&#x3C; EOF | base64 -d > /tmp/SAFE_TEMP_FILE&#xA;</xsl:text>
        <xsl:value-of select="pol:data"/>
        <xsl:text>&#xA;EOF&#xA;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text># unknown element: </xsl:text>
        <xsl:value-of select="name(./*[1])"/>
        <xsl:text>&#xA;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:text>mv /tmp/SAFE_TEMP_FILE </xsl:text>
    <xsl:value-of select="pol:path"/>
    <xsl:text>&#xA;</xsl:text>

    <xsl:text>chown </xsl:text>
    <xsl:value-of select="pol:owner"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="pol:group"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="pol:path"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="pol:run">
    <xsl:variable name="user">
      <xsl:choose>
        <xsl:when test="pol:user != ''">
          <xsl:value-of select="pol:user"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>nobody</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:text>su - </xsl:text>
    <xsl:value-of select="$user"/>
    <xsl:text> '</xsl:text>
    <xsl:value-of select="pol:command"/>
    <xsl:text>'&#xA;</xsl:text>
  </xsl:template>
   
</xsl:stylesheet>
