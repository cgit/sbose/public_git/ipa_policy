<?xml version="1.0" encoding="UTF-8"?>
<!--
Author: Sumit Bose <sbose@redhat.com>

Copyright (C) 2008  Red Hat
see file 'COPYING' for use and warranty information

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; version 2 only

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:md="http://freeipa.org/xsl/metadata/1.0"
  xmlns:seb="http://freeipa.org/xml/rng/selinux_booleans/1.0">

  <md:output_handler>
    <md:file md:name="/tmp/dummy"/>
    <md:exec_with_args md:command="/usr/sbin/setsebool" md:user="root" md:group="root"/>
  </md:output_handler>

  <xsl:param name="output_selector"/>

  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:apply-templates select="seb:ipa"/>
  </xsl:template>

  <xsl:template match="seb:ipa">
    <xsl:apply-templates>
      <xsl:with-param name="seb:seb"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="seb:metadata">
  </xsl:template>

  <xsl:template match="seb:ipaconfig">
    <xsl:apply-templates select="seb:selinux_boolean"/>
  </xsl:template>

  <xsl:template match="seb:selinux_boolean">
    <xsl:value-of select="seb:name"/>
    <xsl:text>=</xsl:text>
    <xsl:value-of select="seb:value"/>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>
   
</xsl:stylesheet>
