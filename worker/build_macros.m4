AC_DEFUN([BUILD_WITH_SHARED_BUILD_DIR],
  [ AC_ARG_WITH([shared-build-dir],
                [AC_HELP_STRING([--with-shared-build-dir=DIR],
                                [temporary build directory where libraries are installed [$srcdir/sharedbuild]])])

    sharedbuilddir="$srcdir/sharedbuild"
    if test x"$with_shared_build_dir" != x; then
        sharedbuilddir=$with_shared_build_dir
        CFLAGS="$CFLAGS -I$with_shared_build_dir/include"
        LDFLAGS="$LDFLAGS -L$with_shared_build_dir/lib"
    fi
    AC_SUBST(sharedbuilddir)
  ])

AC_SUBST(SSSD_CFLAGS)
AC_SUBST(SSSD_LIBS)
AC_DEFUN([BUILD_WITH_SSSD_DIR],
  [ AC_ARG_WITH([sssd-dir],
                [AC_HELP_STRING([--with-sssd-dir=DIR],
                                [path to the place where sssd is build])])

    sssddir="/usr/src/sssd"
    if test x"$with_sssd_dir" != x; then
        sssddir=$with_sssd_dir
        SSSD_CFLAGS="-I$with_sssd_dir/server -I$with_sssd_dir/replace"
        SSSD_LIBS="\
                   $with_sssd_dir/server/util/signal.o \
                   $with_sssd_dir/server/util/server.o \
                   $with_sssd_dir/server/util/memory.o \
                   $with_sssd_dir/server/util/btreemap.o \
                   $with_sssd_dir/server/util/service_helpers.o \
                   $with_sssd_dir/server/confdb/confdb.o \
                   $with_sssd_dir/server/sbus/sssd_dbus_common.o \
                   $with_sssd_dir/server/sbus/sssd_dbus_connection.o \
                   $with_sssd_dir/server/sbus/sssd_dbus_server.o \
                   -ltalloc -lldb -ltevent -ltdb"
        AC_DEFINE([WITH_SSSD],[1],[Define this if you want to build a SSSD client])
    fi
    AC_SUBST(sssddir)
  ])
AM_CONDITIONAL([WITH_SSSD], [test x"$with_sssd_dir" != x])

