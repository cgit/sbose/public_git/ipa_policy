/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _IPAACTION_H_
#define _IPAACTION_H_

int check_ipaaction_condition(const xmlDocPtr doc);
int ipaaction_file(const xmlDocPtr doc);
int ipaaction_run(const xmlDocPtr doc);
int handle_ipaaction(const char *policy_file_name);

#endif /* _IPAACTION_H_ */
