/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _POLICY_COLLECTION_H_
#define _POLICY_COLLECTION_H_

#define XPATH_POLICY_COLLECTION_TEMPLATE "//template"

#define MAX_POLICY_FILES 256

struct policy_collection {
    char *name;
    char *files[MAX_POLICY_FILES];
};

int load_policy_collection(const char *filename, struct policy_collection **pc_array);
int free_policy_collection(struct policy_collection **pc_array);

#endif /* _POLICY_COLLECTION_H_ */
