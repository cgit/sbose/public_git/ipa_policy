/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _HELPERS_H_
#define _HELPERS_H_
struct file_info {
    char *name;
    char *permission;
    char *user;
    char *group;
    char *selinux_context_string;
};

int open_temporary_file(char *name, const char *permission, const char *user, const char *group, const char *selinux_context_string);

int exec_command(const char *command, const char *user, const char *group, char *arguments, char *extra_args);

#endif /* _HELPERS_H_ */
