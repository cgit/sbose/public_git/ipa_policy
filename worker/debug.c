/* taken from Stephen's sssd tree */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

const char *debug_prg_name = "PolicyProcessor";
int debug_level = 5;

void debug_fn(const char *format, ...)
{
    va_list ap;
    char *s = NULL;
    int ret;

    va_start(ap, format);
    ret=vasprintf(&s, format, ap);
    va_end(ap);
    if (ret==-1) {
        fprintf(stderr, "DEBUG_FN: vasprintf failed!!!\n");
        return;
    }

    /*write(state.fd, s, strlen(s)); */
    fprintf(stderr, s);
    free(s);
}
