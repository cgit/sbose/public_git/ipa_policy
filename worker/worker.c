/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <popt.h>


#include "helpers.h"
#include "util.h"
#include "xml_helper.h"
#include "policy_collection.h"
#ifdef WITH_SSSD
#include "sbus_client.h"
#endif


int main(int argc, const char *argv[])
{
    int opt_daemon=0;
    int opt_once=0;
    int opt;
    int opt_check;
    int poli_c;
    int file_c;
    poptContext pc;
    char *policy_file_name=NULL;
    int ret=0;
    struct policy_collection *pc_array;


    struct poptOption long_options[] = {
        POPT_AUTOHELP
        {"daemon", 'D', POPT_ARG_NONE, &opt_daemon, 0, \
         "Become a daemon (default)", NULL }, \
        {"once", 'o', POPT_ARG_NONE, &opt_once, 0, \
         "Run once and process all policies (not a daemon)", NULL}, \
        {"policy", 'p', POPT_ARG_STRING, NULL, 'p', \
         "Only process the given policy", "name of the policy file"}, \
        {"searchpath", 's', POPT_ARG_STRING, NULL, 's', \
         "add the following path to the XML search path (may be used more than once)", "name of a directory"}, \
        POPT_TABLEEND 
    };

    pc = poptGetContext(argv[0], argc, argv, long_options, 0);
    while((opt = poptGetNextOpt(pc)) != -1) {
        switch(opt) {
        case 'p':
            policy_file_name=poptGetOptArg(pc);
            break;
        case 's':
            setup_xml_search_path(poptGetOptArg(pc));
            break;
        default:
            fprintf(stderr, "\nInvalid option %s: %s\n\n",
                    poptBadOption(pc, 0), poptStrerror(opt));
            poptPrintUsage(pc, stderr, 0);
            poptFreeContext(pc);
            ret=1;
            goto cleanup;
        }
    }


    opt_check=0;
    if (opt_daemon != 0 ) opt_check++;
    if (opt_once != 0 ) opt_check++;
    if (policy_file_name != NULL ) opt_check++;

    if (opt_check > 1) {
        fprintf(stderr, "Do not user Option -o|--once, -D|--daemon and -p|--policy together in any combination.\n");
        poptPrintUsage(pc, stderr, 0);
        poptFreeContext(pc);
        ret=1;
        goto cleanup;
    }

    if (opt_check == 0) opt_daemon = 1;

    poptFreeContext(pc);




    setup_xml_search_path("../policy_metadata/");
    
    if ( policy_file_name != NULL ) {
        load_policy_collection("policy_collection_example.xml", &pc_array);

        poli_c=0;
        while (pc_array[poli_c].name != NULL) {
            DEBUG(3,("Schema name for policy collection #%d: %s\n",poli_c, pc_array[poli_c].name));
        
            file_c=0;
            while (pc_array[poli_c].files[file_c] != NULL) {
                DEBUG(3,("   policy file: %s\n",pc_array[poli_c].files[file_c]));
                file_c++;
            }
            poli_c++;
        }
        

        ret=process_policy(policy_file_name);
        if ( ret == -1 ) {
            DEBUG(0,("Invalid policy, aborting.\n"));
            ret=1;
            goto cleanup;
        }
    }

    if (opt_daemon!=0) {
#ifdef WITH_SSSD
        ret = setup_sbus_and_server_loop();
#else
        DEBUG(0,("This binary was not compiled with sssd support.\n"));
#endif
    }

cleanup:
    free_policy_collection(&pc_array);
    free_xml_search_path();
    free(policy_file_name);

    return ret;
}
