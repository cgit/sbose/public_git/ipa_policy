/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _XML_HELPER_H_
#define _XML_HELPER_H_

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/relaxng.h>

#define XMLCHARLEN 255
#define XSLT_METADATA_NAMESPACE (xmlChar *) "http://freeipa.org/xsl/metadata/1.0"
#define XSLT_METADATA_NAMESPACE_PREFIX (xmlChar *) "md"
#define XPATH_OUTPUT_HANDLER (xmlChar *) "//md:output_handler/md:*"

enum {
    FIND_NAME = 1,
    FIND_VALUE
};

extern xmlChar *default_namespace_prefix;

int process_policy(const char *policy_file_name);

xmlChar *get_default_namespace(xmlDocPtr doc);

int validate_policy(const char *policy_file_name, char **ipa_policy_type, char **xslt_file_name);

int print_all_attributes(const xmlNode *node);

char *find_by_xpath(const xmlDocPtr doc, const xmlChar * xpath_expr, const int type);

int setup_xml_search_path(const char *path);
int print_xml_search_path(void);
int free_xml_search_path(void);

#endif /* _XML_HELPER_H_ */
