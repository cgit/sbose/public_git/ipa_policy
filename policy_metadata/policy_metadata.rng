<?xml version="1.0" encoding="utf-8"?>
<!--
Author: Sumit Bose <sbose@redhat.com>

Copyright (C) 2008  Red Hat
see file 'COPYING' for use and warranty information

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; version 2 only

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
-->
<grammar xmlns="http://relaxng.org/ns/structure/1.0"
datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes"
xmlns:a="http://freeipa.org/xml/rng/ns/annotations/1.0"
xmlns:pa="http://freeipa.org/xml/rng/ns/plugable_architecture/1.0">


<define name="policy_metadata">
<element name="metadata">

  <a:doc>The metadata information should be generic for all policies. The RelaxNG schema can be found in a separate file (this file :) and can be included by the schema file of a specific policy with the include pattern. With this separation the policy and the metadata schema can be modified independently and the metadata schema can be used by the UI to render a separate page for the metadata of a policy.</a:doc>

  <element name="name">
    <text/>
  </element>

  <element name="author" pa:edit="no">
    <a:doc>should be added automatically, use login information</a:doc>
    <text/>
  </element>

  <element name="version" pa:edit="no">
    <a:doc>should be added automatically</a:doc>
    <text/>
  </element>

  <element name="RNGfile" pa:label="Name of the RELAX NG file">
    <a:doc>should be added automatically from RelaxNG metadata</a:doc>
    <ref name="rng_filename"/>
  </element>

  <element name="XSLTfile" pa:label="Name of the XSLT file">
    <a:doc>should be added automatically from RelaxNG metadata</a:doc>
    <ref name="xslt_filename"/>
  </element>

  <element name="app" pa:label="Name of the application">
    <a:doc>should be added automatically from RelaxNG metadata</a:doc>
    <ref name="application_name"/>
  </element>

<optional>
  <element name="mergeStrategyXML" pa:label="Howto merge with other policies">
    <choice>
      <value pa:label="Use only this policy">exclusive</value>
      <value pa:label="Merge with other policies">merge</value>
      <value pa:label="Ignore this, if other policies apply">ignore</value>
    </choice>
  </element>

  <element name="mergeStrategyLocal" pa:label="Howto merge with local files">
    <choice>
      <value pa:label="Use only this policy">exclusive</value>
      <value pa:label="Merge with local file">merge</value>
      <value pa:label="Ignore this, if local file exsits">ignore</value>
    </choice>
  </element>

  <element name="description" pa:label="Description, what should the policy do and why, maybe a changelog">
    <text/>
  </element>

</optional>
</element>
</define>
</grammar>
