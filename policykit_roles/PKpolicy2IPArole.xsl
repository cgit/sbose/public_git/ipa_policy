<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


  <xsl:param name="role_name"/>

  <xsl:output method="xml" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:apply-templates select="policyconfig"/>
  </xsl:template>

  <xsl:template match="policyconfig">
    <iparole>
    <role>
    <name>
    <xsl:value-of select="$role_name"/>
    </name>
    <xsl:apply-templates select="action"/>
    </role>
    </iparole>
  </xsl:template>

  <xsl:template match="action">
    <action>
    <action_id>
    <xsl:value-of select="@id"/>
    </action_id>
    <allow_any>
    <xsl:value-of select="allow_any"/>
    </allow_any>
    <allow_inactive>
    <xsl:value-of select="allow_inactive"/>
    </allow_inactive>
    <allow_active>
    <xsl:value-of select="allow_active"/>
    </allow_active>
    </action>
  </xsl:template>
   
</xsl:stylesheet>
