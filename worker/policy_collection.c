/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "util.h"
#include "policy_collection.h"


int load_policy_collection(const char *filename,
                           struct policy_collection **pc_array) {
    int ret=-1;
    int i;
    int pol_file_counter;
    int policy_count;
    xmlDocPtr doc=NULL;
    xmlXPathContextPtr xpath_context=NULL;
    xmlXPathObjectPtr xpath_obj=NULL;
    xmlNodePtr template_node_ptr;
    xmlNodePtr policy_node_ptr;
    

    doc = xmlParseFile(filename);
    CHECK(doc, NULL, ("Cannot parse document %s!\n", filename), return -1);

    ret=validate_with_rng(doc, "policy_collection.rng");
    CHECK(ret, -1, ("Validation of %s failed!\n", filename), goto cleanup);
    ret=-1;
   
    xpath_context = xmlXPathNewContext(doc); 
    CHECK(xpath_context, NULL, ("Error: unable to create new XPath context\n"),
          goto cleanup);

    xpath_obj = xmlXPathEvalExpression(XPATH_POLICY_COLLECTION_TEMPLATE,
                                       xpath_context);
    CHECK(xpath_obj, NULL, ("Unable to evaluate xpath expression \"%s\"\n",
                             XPATH_POLICY_COLLECTION_TEMPLATE), goto cleanup);

    policy_count = xmlXPathNodeSetGetLength(xpath_obj->nodesetval);
    DEBUG(3,("Found %d policy templates in policy collection %s.\n",
             policy_count, filename));

    *pc_array = malloc(sizeof(struct policy_collection) * (policy_count+1));
    CHECK(*pc_array, NULL, ("Malloc failed\n"), goto cleanup);

    for (i=0; i<policy_count; i++) {
        template_node_ptr = xpath_obj->nodesetval->nodeTab[i]->children;
        do {
            if (xmlStrEqual(template_node_ptr->name,(xmlChar *) "name")) {
                DEBUG(3,("Found name: %s\n", template_node_ptr->children->content));
                (*pc_array)[i].name = strdup(template_node_ptr->children->content);
            }

            if (xmlStrEqual(template_node_ptr->name,(xmlChar *) "ordered_policies")) {
                policy_node_ptr = template_node_ptr->children;
                pol_file_counter = 0;
                do {
                    if (xmlStrEqual(policy_node_ptr->name, (xmlChar *) "file")) {
                        DEBUG(3,("Found policy file: %s\n", policy_node_ptr->children->content));
                        (*pc_array)[i].files[pol_file_counter++]=strdup(policy_node_ptr->children->content);
                    }
                    policy_node_ptr = policy_node_ptr->next;
                } while ( policy_node_ptr != template_node_ptr->last); 
                (*pc_array)[i].files[pol_file_counter]=NULL;
            }

            template_node_ptr = template_node_ptr->next;
        } while ( template_node_ptr != xpath_obj->nodesetval->nodeTab[i]->last);
    }

    (*pc_array)[i].name=NULL; 

    ret=0;

cleanup:
    xmlXPathFreeObject(xpath_obj);
    xmlXPathFreeContext(xpath_context);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    return ret;
}

int free_policy_collection(struct policy_collection **pc_array) {
    int poli_c;
    int file_c;

    poli_c=0;
    while ((*pc_array)[poli_c].name != NULL) {
        free((*pc_array)[poli_c].name);
        file_c=0;
        while ((*pc_array)[poli_c].files[file_c] != NULL) {
            free((*pc_array)[poli_c].files[file_c]);
            file_c++;
        }
        poli_c++;
    }
    free(*pc_array);
}
