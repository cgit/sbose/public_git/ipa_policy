/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __WORKER_UTIL_H__
#define __WORKER_UTIL_H__

extern int debug_level;
void debug_fn(const char *format, ...);

#include <malloc.h>
#define MEMINFO do{ \
    struct mallinfo minfo; \
    minfo=mallinfo(); \
    DEBUG(5, ("---allocated space: %d-----\n",minfo.uordblks)); \
}while(0)

#define DEBUG(level, body) do { \
    if (level <= debug_level) { \
        debug_fn("DEBUG-%d (%s,%d): %s: ", level, __FILE__, __LINE__ , __FUNCTION__); \
        debug_fn body; \
    } \
} while(0)


#define CHECK(pointer, val, message, action) do { \
    if (pointer == (val)) { \
        DEBUG(0, message); \
        action; \
    } \
} while(0)

#define CHECK_NULL_FATAL(pointer, message) do { \
    if (pointer == NULL) { \
        DEBUG(0, message); \
        exit(1); \
    } \
} while(0)

#define CHECK_NULL_RETURN(pointer, message) do { \
    if (pointer == NULL) { \
        DEBUG(0, message); \
        return(-1); \
    } \
} while(0)

#define CHECK_MINUS_ONE_RETURN(pointer, message) do { \
    if (pointer == -1) { \
        DEBUG(0, message); \
        return(-1); \
    } \
} while(0)

#endif /* __WORKER_UTIL_H__ */

