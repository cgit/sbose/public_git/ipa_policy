<?xml version="1.0" encoding="UTF-8"?>
<!--
Author: Sumit Bose <sbose@redhat.com>

Copyright (C) 2008  Red Hat
see file 'COPYING' for use and warranty information

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; version 2 only

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:md="http://freeipa.org/xsl/metadata/1.0"
  xmlns:xd="http://www.pnp-software.com/XSLTdoc"
  xmlns:pse="http://freeipa.org/xml/rng/pam_selinux_roles/1.0">

  <md:output_handler>
    <md:file md:name="/tmp/pam_selinux-SAFE.ldif" md:owner="root" md:group="root" md:permission="400"/>
  </md:output_handler>

  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:text># IPA generated ldif for pam_selinux roles. DO NOT EDIT&#xA;&#xA;</xsl:text>
    <xsl:apply-templates select="pse:ipa"/>
  </xsl:template>

  <xsl:template match="pse:ipa">
    <xsl:apply-templates>
      <xsl:with-param name="pse:iparole"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="pse:metadata">
  </xsl:template>

  <xsl:template match="pse:iparole">
      <xsl:apply-templates select="pse:role"/>
  </xsl:template>

  <xsl:template match="pse:role">
    <xsl:text># role: </xsl:text>
    <xsl:value-of select="pse:name"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>dn: ou=</xsl:text>
    <xsl:value-of select="pse:name"/>
    <xsl:text>,ou=pam_selinux_roles,xx=some,xx=ldap,xx=path&#xA;&#xA;&#xA;</xsl:text>

    <xsl:apply-templates select="pse:default_context">
      <xsl:with-param name="rolename" select="pse:name"/>
    </xsl:apply-templates>

    <xsl:apply-templates select="pse:context">
      <xsl:with-param name="rolename" select="pse:name"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="pse:default_context">
    <xsl:param name="rolename"/>
    <xsl:call-template name="context_start">
      <xsl:with-param name="rolename" select="$rolename"/>
      <xsl:with-param name="selinux_user" select="pse:selinux_user"/>
      <xsl:with-param name="mls" select="pse:mls"/>
    </xsl:call-template>
    <xsl:text>&#xa;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="pse:context">
    <xsl:param name="rolename"/>
    <xsl:call-template name="context_start">
      <xsl:with-param name="rolename" select="$rolename"/>
      <xsl:with-param name="selinux_user" select="pse:selinux_user"/>
      <xsl:with-param name="mls" select="pse:mls"/>
    </xsl:call-template>
    <xsl:apply-templates select="pse:service">
      <xsl:with-param name="rolename" select="pse:name"/>
    </xsl:apply-templates>
    <xsl:text>&#xa;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="pse:service">
    <xsl:if test="position()=1">
      <xsl:text>-&#xa;changetype: modify&#xA;</xsl:text>
      <xsl:text>replace: service&#xA;</xsl:text>
    </xsl:if>
    <xsl:text>service: </xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>

  <xsl:template name="context_start">
    <xsl:param name="rolename"/>
    <xsl:param name="selinux_user"/>
    <xsl:param name="mls"/>
    <xsl:text>dn: ou=</xsl:text>
    <xsl:value-of select="$selinux_user"/>
    <xsl:text>,ou=</xsl:text>
    <xsl:value-of select="$rolename"/>
    <xsl:text>,ou=pam_selinux_roles,xx=some,xx=ldap,xx=path&#xA;</xsl:text>
    <xsl:text>changetype: modify&#xA;</xsl:text>
    <xsl:text>replace: selinux_user&#xA;selinux_user: </xsl:text>
    <xsl:value-of select="$selinux_user"/>
    <xsl:if test="$mls != ''">
      <xsl:text>&#xa;-&#xa;changetype: modify&#xA;</xsl:text>
      <xsl:text>replace: mls&#xA;mls: </xsl:text>
      <xsl:value-of select="$mls"/>
      <xsl:text>&#xa;</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="pse:action">
    <xsl:param name="rolename"/>

    <xsl:text>dn: ou=</xsl:text>
    <xsl:value-of select="pse:action_id"/>
    <xsl:text>,ou=</xsl:text>
    <xsl:value-of select="$rolename"/>
    <xsl:text>,ou=PolicyKitRoles,xx=some,xx=ldap,xx=path&#xA;</xsl:text>
    <xsl:text>changetype: modify&#xA;</xsl:text>
    <xsl:text>replace: allow_any&#xA;allow_any: </xsl:text>
    <xsl:value-of select="pse:allow_any"/>
    <xsl:text>&#xa;-&#xa;changetype: modify&#xA;</xsl:text>
    <xsl:text>replace: allow_inactive&#xA;allow_inactive: </xsl:text>
    <xsl:value-of select="pse:allow_inactive"/>
    <xsl:text>&#xa;-&#xa;changetype: modify&#xA;</xsl:text>
    <xsl:text>replace: allow_active&#xA;allow_active: </xsl:text>
    <xsl:value-of select="pse:allow_active"/>
    <xsl:text>&#xa;&#xA;</xsl:text>
  </xsl:template>
   
</xsl:stylesheet>
