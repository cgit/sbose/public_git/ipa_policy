/*
 * Copyright (C) Sumit Bose 2009
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _OUTPUT_HANDLER_H_
#define _OUTPUT_HANDLER_H_

char *get_output_handler_parameter(xmlNode *node, const char *name, const char *default_value, const int required);
int output_handler_file(xmlNode *node, const xmlDocPtr doc, const char *xslt_file_name);
int output_handler_exec_with_args(xmlNode *node, const xmlDocPtr doc, const char *xslt_file_name);
int find_output_handler(const char *policy_file_name, const char *xslt_file_name);

#endif /* _OUTPUT_HANDLER_H_ */
