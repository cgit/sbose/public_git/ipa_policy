<?xml version="1.0" encoding="UTF-8"?>
<!--
Author: Sumit Bose <sbose@redhat.com>

Copyright (C) 2008  Red Hat
see file 'COPYING' for use and warranty information

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; version 2 only

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:md="http://freeipa.org/xsl/metadata/1.0"
  xmlns:xd="http://www.pnp-software.com/XSLTdoc"
  xmlns:ipaaction="http://freeipa.org/xml/rng/ipaaction/1.0">

  <md:output_handler>
    <xd:doc>Here we have two different kind of handlers 'download' and 'exec'.</xd:doc>
    <md:download md:param_name="output_selector" md:param_value="file"/>
    <xd:doc>TODO: we have to decide what the client application should do. It would be possible to call an external program like 'curl' or 'wget' or the call libcurl to download a file. I would vote for using 'curl' or 'libcurl' because it seem that curl supports  more methods than wget. Download should be done by user nobody into a teporary file and then moved and chowned to the destination.</xd:doc>
    <md:exec md:param_name="output_selector" md:param_value="run"/>
    <xd:doc>TODO: we have to decide how the client application should call the applied program or script. If no 'user' is specifed the default user should be 'nobody'.</xd:doc>
  </md:output_handler>

  <xsl:param name="output_selector"/>

  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:text># IPA generated script for ipaaction policy. DO NOT EDIT&#xA;&#xA;</xsl:text>
    <xsl:apply-templates select="ipaaction:ipa"/>
  </xsl:template>

  <xsl:template match="ipaaction:ipa">
    <xsl:apply-templates>
      <xsl:with-param name="ipaaction:ipaaction"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="ipaaction:metadata">
  </xsl:template>

  <xsl:template match="ipaaction:ipaaction">
    <xsl:choose>
      <xsl:when test="$output_selector='file'">
        <xsl:apply-templates select="ipaaction:file"/>
      </xsl:when>
      <xsl:when test="$output_selector='run'">
        <xsl:apply-templates select="ipaaction:run"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text># unknown output_selector&#xA;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="ipaaction:file">
    <xsl:choose>
      <xsl:when test="name(./*[1])='url'">
        <xsl:text>su - nobody 'curl -o /tmp/SAFE_TEMP_FILE </xsl:text>
        <xsl:value-of select="ipaaction:url"/>
        <xsl:text>'&#xA;</xsl:text>
      </xsl:when>
      <xsl:when test="name(./*[1])='data'">
        <xsl:text>cat &#x3C;&#x3C; EOF | base64 -d > /tmp/SAFE_TEMP_FILE&#xA;</xsl:text>
        <xsl:value-of select="ipaaction:data"/>
        <xsl:text>&#xA;EOF&#xA;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text># unknown element: </xsl:text>
        <xsl:value-of select="name(./*[1])"/>
        <xsl:text>&#xA;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:text>mv /tmp/SAFE_TEMP_FILE </xsl:text>
    <xsl:value-of select="ipaaction:path"/>
    <xsl:text>&#xA;</xsl:text>

    <xsl:text>chown </xsl:text>
    <xsl:value-of select="ipaaction:owner"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="ipaaction:group"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="ipaaction:path"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="ipaaction:run">
    <xsl:variable name="user">
      <xsl:choose>
        <xsl:when test="ipaaction:user != ''">
          <xsl:value-of select="ipaaction:user"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>nobody</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:text>su - </xsl:text>
    <xsl:value-of select="$user"/>
    <xsl:text> '</xsl:text>
    <xsl:value-of select="ipaaction:command"/>
    <xsl:text>'&#xA;</xsl:text>
  </xsl:template>
   
</xsl:stylesheet>
